# Shortify

## Prerequisites
Docker, when running locally JDK17, Maven, MongoDB

## Description

### Installation

To build the project locally run
```
./mvnw package
```

The application can be run using docker-compose using
```
docker-compose up
```

The Api documentation can be found at http://localhost:8080/swagger-ui/index.html#/
