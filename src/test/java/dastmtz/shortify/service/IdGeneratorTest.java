package dastmtz.shortify.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Base64;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class IdGeneratorTest {

    @Autowired
    private IdGenerator idGenerator;

    @Test
    @DisplayName("Verify 'createId' creates different ids for different inputs.")
    public void createId_differentUrls() {
        String id1 = idGenerator.createId("SomeText");
        String id2 = idGenerator.createId("DifferentText");

        assertNotEquals(id1, id2);
    }

    @Test
    @DisplayName("Verify 'createId' creates the same ids for equal inputs.")
    public void createId_sameUrls() {
        String id1 = idGenerator.createId("SomeText");
        String id2 = idGenerator.createId("SomeText");

        assertEquals(id1, id2);
    }

    @Test
    @DisplayName("Verify the 'createId' returns a Base64 encoded string.")
    void createId_returnsBase64EncodedString() {
        String id = idGenerator.createId("SomeText");

        assertDoesNotThrow(() -> Base64.getDecoder().decode(id));
    }

}