package dastmtz.shortify.service;

import dastmtz.shortify.exceptions.ShortlinkCollisionException;
import dastmtz.shortify.model.UrlMapping;
import dastmtz.shortify.repository.UrlMappingRepository;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MappingServiceTest {
    @Mock
    IdGenerator idGenerator;

    @Mock
    UrlMappingRepository repository;

    @InjectMocks
    MappingService service;


    @Nested
    class TestCreateUrlShortlink {

        @Test
        @DisplayName("Verify `createUrlShortlink` uses ShortlinkGenerator to create unique id.")
        void createUrlShortlink_createsUniqueId() {
            service.createUrlShortlink("example.com");

            verify(idGenerator, times(1)).createId(any());
        }

        @SneakyThrows
        @Test
        @DisplayName("Verify `createUrlShortlink` passes expected data to repo for persistance.")
        void createUrlShortlink_persistance() {
            UrlMapping expectedMapping = new UrlMapping("example.com", "123");
            when(idGenerator.createId(any())).thenReturn(expectedMapping.shortlink());
            when(repository.saveUrlMapping(any(UrlMapping.class))).thenReturn(expectedMapping);

            UrlMapping result = service.createUrlShortlink(expectedMapping.url());

            verify(repository, times(1)).saveUrlMapping(any(UrlMapping.class));
            assertEquals(expectedMapping, result);
        }

        @SneakyThrows
        @Test
        @DisplayName("Verify `createUrlShortlink` handles collisions of shortlink.")
        void createUrlShortlink_collisionHandling() {
            UrlMapping expectedMapping = new UrlMapping("example.com", "1234");
            when(repository.saveUrlMapping(any())).thenThrow(ShortlinkCollisionException.class);
            when(idGenerator.createExtendedId(any())).thenReturn(expectedMapping.shortlink());
            when(repository.insert(any(UrlMapping.class))).thenReturn(expectedMapping);

            UrlMapping result = service.createUrlShortlink(expectedMapping.url());

            verify(repository, times(1)).saveUrlMapping(any());
            verify(repository, times(1)).insert(any(UrlMapping.class));

            assertEquals(expectedMapping, result);
        }
    }

    @Nested
    class TestResolveUrl {
        @Test
        @DisplayName("Verify `resolveUrl` calls repository to retrieve mapping and returns result.")
        void resolveUrl_callsRepo_andReturnsMapping() {
            UrlMapping expectedMapping = new UrlMapping("example.com", "1234");
            when(repository.findByShortlink(anyString())).thenReturn(expectedMapping);

            UrlMapping result = service.resolveUrl(expectedMapping.url());

            verify(repository, times(1)).findByShortlink(any());
            assertEquals(expectedMapping, result);
        }
    }
}