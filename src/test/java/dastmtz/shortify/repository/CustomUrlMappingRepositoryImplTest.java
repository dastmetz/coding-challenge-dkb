package dastmtz.shortify.repository;

import dastmtz.shortify.exceptions.ShortlinkCollisionException;
import dastmtz.shortify.model.UrlMapping;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doThrow;

@ExtendWith(MockitoExtension.class)
class CustomUrlMappingRepositoryImplTest {
    @Mock
    MongoTemplate mongoTemplate;

    @InjectMocks
    CustomUrlMappingRepositoryImpl customUrlMappingRepository;


    @Test
    @DisplayName("Verify `saveUrlMapping` throws ShortlinkCollisionException if shortlink already exists")
    void saveUrlMapping_ShortlinkCollisionException() {
        UrlMapping urlMapping = Mockito.mock(UrlMapping.class);
        doThrow(new DuplicateKeyException("Duplicate key error with _id")).when(mongoTemplate).save(urlMapping);

        assertThrows(ShortlinkCollisionException.class, () -> {
            customUrlMappingRepository.saveUrlMapping(urlMapping);
        });
    }

    @Test
    @DisplayName("Verify `saveUrlMapping` rethrows other DuplicateKeyExceptions.")
    void saveUrlMapping_OtherExceptions() {
        UrlMapping urlMapping = Mockito.mock(UrlMapping.class);
        doThrow(new DuplicateKeyException("Some other exception")).when(mongoTemplate).save(urlMapping);

        assertThrows(RuntimeException.class, () -> {
            customUrlMappingRepository.saveUrlMapping(urlMapping);
        });
    }

}