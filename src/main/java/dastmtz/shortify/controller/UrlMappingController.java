package dastmtz.shortify.controller;

import dastmtz.shortify.model.UrlMapping;
import dastmtz.shortify.model.UrlMappingRequest;
import dastmtz.shortify.model.UrlMappingResponse;
import dastmtz.shortify.service.MappingService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(UrlMappingController.BASE_URL)
public class UrlMappingController {

    public static final String BASE_URL = "/api/v1";
    private final MappingService mappingService;

    public UrlMappingController(MappingService mappingService) {
        this.mappingService = mappingService;
    }

    @Operation(
            summary = "Create a new UrlMapping."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "UrlMapping created",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = UrlMappingResponse.class))
                    }),
            @ApiResponse(responseCode = "400", description = "Invalid url supplied", content = @Content),
    })
    @GetMapping("/{id}")
    @PostMapping("/urlMapping")
    public UrlMappingResponse createUrlMapping(@RequestBody @Valid UrlMappingRequest input) {
        UrlMapping mapping = mappingService.createUrlShortlink(input.getUrl());
        return new UrlMappingResponse(mapping);
    }

    @Operation(
            summary = "Resolve a URL by it's shortlink."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Url resolved successfully",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = UrlMappingResponse.class))
                    }),
            @ApiResponse(responseCode = "404", description = "Unknown shortlink supplied", content = @Content),
    })
    @GetMapping("/urlMapping/{shortURL}")
    @ResponseStatus(HttpStatus.OK)
    public UrlMappingResponse getUrlMappingByShortlink(@PathVariable String shortURL) {
        UrlMapping mapping = mappingService.resolveUrl(shortURL);
        return new UrlMappingResponse(mapping);
    }

}
