package dastmtz.shortify.exceptions;

public class ShortlinkCollisionException extends Exception {
    public ShortlinkCollisionException(String message) {
        super(message);
    }

}
