package dastmtz.shortify.repository;

import dastmtz.shortify.exceptions.ShortlinkCollisionException;
import dastmtz.shortify.model.UrlMapping;

public interface CustomUrlMappingRepository {
    UrlMapping saveUrlMapping(UrlMapping entity) throws ShortlinkCollisionException;
}
