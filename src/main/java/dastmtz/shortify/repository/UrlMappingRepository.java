package dastmtz.shortify.repository;

import dastmtz.shortify.model.UrlMapping;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UrlMappingRepository extends MongoRepository<UrlMapping, String>, CustomUrlMappingRepository {
    UrlMapping findByShortlink(String shortlink);
}
