package dastmtz.shortify.repository;

import dastmtz.shortify.exceptions.ShortlinkCollisionException;
import dastmtz.shortify.model.UrlMapping;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;

@Slf4j
public class CustomUrlMappingRepositoryImpl implements CustomUrlMappingRepository {
    private final MongoTemplate mongoTemplate;

    public CustomUrlMappingRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public UrlMapping saveUrlMapping(UrlMapping entity) throws ShortlinkCollisionException {
        try {
            return mongoTemplate.save(entity);
        } catch (DuplicateKeyException e) {
            log.warn("{} -> {}", e.getClass(), e.getMessage());
            if (e.getMessage().contains("_id")) {
                throw new ShortlinkCollisionException("Shortlink collision");
            }
            throw e;
        }
    }
}
