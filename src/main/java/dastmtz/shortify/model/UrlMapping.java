package dastmtz.shortify.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "urls")
public record UrlMapping(
        @Indexed(unique = true)
        String url,
        @Id
        String shortlink) {
}
