package dastmtz.shortify.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UrlMappingRequest {
    @NotNull
    @NotBlank
    @Schema(
            description ="URL that should be shortened."
    )
    String url;
}
