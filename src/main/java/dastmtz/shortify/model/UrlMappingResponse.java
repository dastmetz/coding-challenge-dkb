package dastmtz.shortify.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NonNull;

@Data
public class UrlMappingResponse {
    @NonNull
    @Schema(description = "Original URL that was shortened.")
    String url;
    @NonNull
    @Schema(description = "Alias that maps to the original URL.")
    String shortlink;

    public UrlMappingResponse(UrlMapping mapping){
        this.url = mapping.url();
        this.shortlink = mapping.shortlink();
    }
}
