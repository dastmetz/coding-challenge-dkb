package dastmtz.shortify.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
public class IdGenerator {

    @Value("${idGeneratorDefaultLength:4}")
    private Integer defaultIdLength;

    @Value("${idGeneratorExtendedLength:8}")
    private Integer extendedIdLength;


    /**
     * Return a unique, base64 encoded id for the given input.
     * Number of characters is specified by {@link IdGenerator#defaultIdLength}.
     */
    public String createId(String text) {
        return createIdWithLength(text, defaultIdLength);
    }

    /**
     * Return a unique, base64 encoded id for the given input.
     * Number of characters is specified by {@link IdGenerator#extendedIdLength}.
     */
    public String createExtendedId(String text) {
        return createIdWithLength(text, extendedIdLength);
    }

    private String createIdWithLength(String text, int length) {
        String id = DigestUtils.sha256Hex(text);
        return Base64.getEncoder().encodeToString(id.getBytes()).substring(0, length);
    }

}
