package dastmtz.shortify.service;

import dastmtz.shortify.exceptions.ShortlinkCollisionException;
import dastmtz.shortify.model.UrlMapping;
import dastmtz.shortify.repository.UrlMappingRepository;
import org.springframework.stereotype.Service;

@Service
public class MappingService {
    private final IdGenerator idGenerator;
    private final UrlMappingRepository repository;

    public MappingService(IdGenerator idGenerator, UrlMappingRepository repository) {
        this.idGenerator = idGenerator;
        this.repository = repository;
    }

    public UrlMapping createUrlShortlink(String fullUrl) {
        String shortlink = idGenerator.createId(fullUrl);
        UrlMapping urlMapping = new UrlMapping(fullUrl, shortlink);
        try {
            repository.saveUrlMapping(urlMapping);
        } catch (ShortlinkCollisionException e) {
            urlMapping = insertEnhancedShortlink(fullUrl);
        }
        return urlMapping;
    }

    public UrlMapping resolveUrl(String shortURL) {
        return repository.findByShortlink(shortURL);
    }

    private UrlMapping insertEnhancedShortlink(String fullUrl) {
        String shortLink = idGenerator.createExtendedId(fullUrl);
        UrlMapping mapping = new UrlMapping(fullUrl, shortLink);
        return repository.insert(mapping);
    }


}
