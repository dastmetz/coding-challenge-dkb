FROM  maven:3.9-eclipse-temurin-17 as builder
WORKDIR /opt/demo
COPY pom.xml .
COPY src src
RUN mvn  install -DskipTests

FROM eclipse-temurin:17-jre-jammy
WORKDIR /opt/demo
COPY --from=builder /opt/demo/target/*.jar /opt/demo/shortify.jar

CMD ["java", "-jar", "shortify.jar"]